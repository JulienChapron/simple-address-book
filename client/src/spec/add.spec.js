
import Add from '../components/add.svelte'
import renderWithRouter from "./renderWithRouter";

describe('add.svelte', () => {
  it('should render component', () => {
    const { getByTestId } = renderWithRouter(Add);
    const element = getByTestId('add-button');
    expect(element).not.toBeNull()
  })
})