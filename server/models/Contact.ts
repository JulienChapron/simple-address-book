import { db } from "../database/db.ts";


interface Contact {
  _id: { $oid: string };
  firstname: string;
  lastname: string;
  email: string;
  mobile: string;
  avatar: string;
  user_id: string
}
const contacts = db.collection<Contact>("contacts");

export { contacts };
