import * as bcrypt from "https://deno.land/x/bcrypt@v0.2.4/mod.ts";
import { create } from "https://deno.land/x/djwt@v2.4/mod.ts";
import { key, header } from "../utils/jwt.ts";
import { users } from "../models/Authentification.ts";

export const signup = async ({ request, response }: {
  request: any;
  response: any;
  cookies: any;
}) => {
  try {
    if (!request.hasBody) {
      response.status = 400;
      response.body = {
        success: false,
        msg: "An error has been occured",
      };
    } else {
      const body = await request.body({ type: "form-data" });
      const data = await body.value.read();
      if (!data.fields.username || !data.fields.password) {
        response.status = 200;
        response.body = {
          success: false,
          msg: "All form fields must be filled in",
        };
      }
      else if (await users.findOne({ username: data.fields.username })) {
        response.status = 200;
        response.body = {
          success: false,
          msg: "Username already exists",
        };
      } else {
        const user = await users.insertOne({
          username: data.fields.username,
          password: await bcrypt.hashSync(data.fields.password),
        });
        response.status = 201;
        response.body = {
          success: true,
          msg: "Successfully registered",
        };
      }
    }
  } catch (err) {
    response.status = 400;
    response.body = {
      success: false,
      msg: err.toString(),
    };
  }

}

export const login = async ({ request, response }: {
  request: any;
  response: any;
}) => {
  try {
    if (!request.hasBody) {
      response.status = 400;
      response.body = {
        success: false,
        msg: "An error has been occured",
      };
    }
    else {
      const body = await request.body({ type: "form-data" });
      const data = await body.value.read();
      const user = await users.findOne({ username: data.fields.username });
      const keyAwait = await key
      if (!data.fields.username || !data.fields.password) {
        response.status = 200;
        response.body = {
          success: false,
          msg: "All form fields must be filled in",
        };
      }
      else if (user) {
        if (await bcrypt.compareSync(data.fields.password, user.password)) {
          const token = await create(header, data, keyAwait)
          response.status = 201;
          response.body = {
            success: true,
            data: {
              username: user.username,
              _id: user._id,
              token: token,
              isAuthenticated: true
            },
            msg: "Successfully connected",
          }
        }
        else {
          response.status = 200;
          response.body = {
            success: false,
            msg: "Wrong Password",
          };
        }
      }
      else {
        response.status = 200;
        response.body = {
          success: false,
          msg: "Username does not exist",
        };
      }
    }
  } catch (err) {
    response.status = 400;
    console.log(err)
    response.body = {
      success: false,
      msg: "An error has been occured",
    };
  }
}
